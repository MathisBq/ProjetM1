#!/usr/bin/env python
# coding: utf-8

# In[2]:


import subprocess

# Lancer main.py dans un processus séparé
main_process = subprocess.Popen(['python', 'run.py'])

# Lancer run.py dans un processus séparé
run_process = subprocess.Popen(['python', 'run1.py'])

# Attendre que les deux processus se terminent
main_process.wait()
run_process.wait()


# In[ ]:




