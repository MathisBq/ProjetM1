import tkinter as tk
from tkinter import filedialog
from functools import partial
import cv2
import os
from ultralytics import YOLO
import math
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import subprocess

def sauvegarder_infos_video(video_path, output_folder, frames_number, video_name, duration):
    # Obtention de l'ID de la vidéo
    video_id = ''
    found_alpha = False
    for char in video_name:
        if char.isalpha() and found_alpha < 2:
            video_id += char.lower()
            found_alpha = found_alpha + 1
        elif char.isdigit():
            video_id += char
        elif not(char.isalpha()):
            break

    # Extraction du nom de la vidéo (en supprimant l'extension)
    video_nom = video_name.split(".")[0]

    # Extraction du format de la vidéo (extension)
    video_format = video_name.split(".")[-1]

    # Configuration de l'authentification pour Google Sheets
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('projetm1-417108-4f9a10581282.json', scope)
    client = gspread.authorize(credentials)
    sheet = client.open_by_key('1Or2WKRekFkoEeb6PlFUOKT0ppKm3xKqlY2HAKdcbp5c').worksheet('vidéos')  # Ouverture de la troisième feuille du Google Sheet

    # Enregistrement des informations dans le Google Sheet
    sheet.append_row([video_id, video_nom, duration, 1, video_format, video_path])  # Ajout des informations dans la feuille
    
    return video_id

def video_to_frames(video_path, output_folder, frames_number): 
    frames_folder = os.path.join(output_folder, 'frames')
    
    if not os.path.exists(frames_folder):
        os.makedirs(frames_folder)
    
    # Chargement de la vidéo
    video_capture = cv2.VideoCapture(video_path)

    # Calcul du pas entre chaque frame pour obtenir le nombre requis
    total_frames = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = total_frames / video_capture.get(cv2.CAP_PROP_FPS)  # Durée totale de la vidéo en secondes
    frames_indices = [int(i * (total_frames - 1) / (frames_number - 1)) for i in range(frames_number)]
    
    video_name = os.path.basename(video_path)
    
    video_id = sauvegarder_infos_video(video_path, output_folder, frames_number, video_name, duration)

    # Lecture et extraction des frames
    count = 0  # Compteur pour nommer les frames
    extracted_frames = 0
    frames_times = []  # Liste pour stocker les temps de chaque frame

    while True:
        success, image = video_capture.read()

        if not success:
            break

        if count in frames_indices:
            frame_path = os.path.join(frames_folder, f"frame{extracted_frames}.jpg")
            cv2.imwrite(frame_path, image)
            # Calcul du temps pour cette frame
            frame_time = (count / total_frames) * duration  # Temps en secondes
            frames_times.append(frame_time)  # Ajouter le temps de cette frame à la liste
            extracted_frames += 1

        count += 1

    # Libération de la ressource vidéo
    video_capture.release()

    return frames_times, video_id  # Retourner la liste des temps de chaque frame

def calculer_3angle(A, B, C):
    # Calcul des vecteurs entre les points
    vecteur_B_A = [B[0] - A[0], B[1] - A[1]]
    vecteur_B_C = [B[0] - C[0], B[1] - C[1]]

    # Calcul de la norme des vecteurs
    norme_B_A = math.sqrt(vecteur_B_A[0] ** 2 + vecteur_B_A[1] ** 2)
    norme_B_C = math.sqrt(vecteur_B_C[0] ** 2 + vecteur_B_C[1] ** 2)

    # Produit scalaire des vecteurs
    produit_scalaire = vecteur_B_A[0] * vecteur_B_C[0] + vecteur_B_A[1] * vecteur_B_C[1]

    # Calcul de l'angle en radians
    angle_radians = math.acos(produit_scalaire / (norme_B_A * norme_B_C))

    # Conversion de l'angle en degrés
    angle_degrees = math.degrees(angle_radians)

    return angle_degrees

def calculer_angle_dos(eg, ed, hg, hd):
    # Calcul des vecteurs entre les épaules et les hanches
    vecteur_eg_ed = [ed[0] - eg[0], ed[1] - eg[1]]
    vecteur_hg_hd = [hd[0] - hg[0], hd[1] - hg[1]]

    # Calcul de la norme des vecteurs
    norme_eg_ed = math.sqrt(vecteur_eg_ed[0] ** 2 + vecteur_eg_ed[1] ** 2)
    norme_hg_hd = math.sqrt(vecteur_hg_hd[0] ** 2 + vecteur_hg_hd[1] ** 2)

    # Produit scalaire des vecteurs
    produit_scalaire = vecteur_eg_ed[0] * vecteur_hg_hd[0] + vecteur_eg_ed[1] * vecteur_hg_hd[1]

    # Calcul de l'angle en radians
    angle_radians = math.acos(produit_scalaire / (norme_eg_ed * norme_hg_hd))

    # Conversion de l'angle en degrés
    angle_degrees = math.degrees(angle_radians)

    return angle_degrees

def liste_poses(keypoints_list):
    poses = []

    for keypoints_set in keypoints_list:
        pose = {}

        # Liste de pose
        pose['right leg angle'] = round(calculer_3angle(keypoints_set[16], keypoints_set[14], keypoints_set[12]), 2)
        pose['left leg angle'] = round(calculer_3angle(keypoints_set[15], keypoints_set[13], keypoints_set[11]), 2)
        pose['right arm angle'] = round(calculer_3angle(keypoints_set[10], keypoints_set[8], keypoints_set[6]), 2)
        pose['left arm angle'] = round(calculer_3angle(keypoints_set[9], keypoints_set[7], keypoints_set[5]), 2)
        pose['back angle'] = round(calculer_angle_dos(keypoints_set[5], keypoints_set[6], keypoints_set[11], keypoints_set[12]), 2)
        
        # Ajouter le symbole degré après chaque valeur d'angle
        for key, value in pose.items():
            pose[key] = str(value) + "°"

        poses.append(pose)

    return poses

def sauvegarder_keypoints(output_folder, video_path, frames_number):
    # Charger le modèle pré-entraîné YOLO
    model = YOLO('yolov8n-pose.pt')
    
    # Configuration de l'authentification pour Google Sheets
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('projetm1-417108-4f9a10581282.json', scope)
    client = gspread.authorize(credentials)
    sheet = client.open_by_key('1Or2WKRekFkoEeb6PlFUOKT0ppKm3xKqlY2HAKdcbp5c').sheet1  # Ouvrir la première feuille du Google Sheet
    
    dossier_images = os.path.join(output_folder, 'frames')
    
    frame_number = 1
    frames_times, video_id = video_to_frames(video_path, output_folder, frames_number)  # Obtenez la liste des temps de chaque frame
    
    for nom_fichier in os.listdir(dossier_images):
        if nom_fichier.endswith(".jpg") or nom_fichier.endswith(".jpeg") or nom_fichier.endswith(".png"):
            # Chemin complet de l'image
            chemin_image = os.path.join(dossier_images, nom_fichier)

            # Obtenir les résultats pour l'image
            results = model(chemin_image)[0]

            # Récupérer les keypoints à partir des résultats
            keypoints = results.keypoints.xy

            # Convertir les keypoints en liste
            keypoints_list = keypoints.tolist()

            # Enregistrer les keypoints dans une liste de dictionnaires pour chaque frame
            keypoints_data = []
            for point_set in keypoints_list:
                frame_data = {'Frame': f'frame_{frame_number}'}
                for j, point in enumerate(point_set):
                    frame_data[f'{keypoints_names[j]}_X'] = round(point[0], 2)  # Arrondir à deux chiffres après la virgule
                    frame_data[f'{keypoints_names[j]}_Y'] = round(point[1], 2)  # Arrondir à deux chiffres après la virgule
                keypoints_data.append(frame_data)
                frame_number += 1

            # Appeler la fonction liste_poses pour obtenir les poses
            poses = liste_poses(keypoints_list)

            # Obtenir le temps de la frame actuelle
            frame_time = round(frames_times[frame_number - 2], 2)  # frame_number - 2 car frame_number commence à 1

            # Écrire les données de keypoints, poses et temps dans le Google Sheet
            for i, data in enumerate(keypoints_data):
                row_data = [data.get('Frame'), frame_time, video_id]  # Vidéo ID en troisième position
                for keypoint_name in keypoints_names:
                    row_data.extend([data.get(f'{keypoint_name}_X', ''), data.get(f'{keypoint_name}_Y', '')])

                # Ajouter chaque pose dans une colonne distincte
                row_data.extend([poses[i].get('right leg angle', ''), poses[i].get('left leg angle', ''), 
                                 poses[i].get('right arm angle', ''), poses[i].get('left arm angle', ''),
                                 poses[i].get('back angle', '')])

                sheet.append_row(row_data)


keypoints_names = [
    'nez', 'oeil_gauche', 'oeil_droit', 'oreille_gauche', 'oreille_droite', 
    'epaule_gauche', 'epaule_droite', 'coude_gauche', 'coude_droit', 
    'poignet_gauche', 'poignet_droit', 'hanche_gauche', 'hanche_droite', 
    'genou_gauche', 'genou_droit', 'cheville_gauche', 'cheville_droite'
]

"""
Points clés: 17 points clés
nez, oeil gauche, oeil droit, oreille gauche, oreille droite, épaule gauche, épaule droite, coude gauche, coude droit,
poignet gauche, poignet droit, hanche gauche, hanche droite, genou gauche, genou droit, cheville gauche, cheville droite.
"""

video_path = ""
output_folder = ""
frames_number = 0

def toggle_part():
    if selected_part.get() == "tête":
        body_checkbox.deselect()
    else:
        head_checkbox.deselect()

def run_application(video_path, output_folder, frames_number_entry, selected_part):
    frames_number = int(frames_number_entry)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if selected_part == "tête":
        subprocess.run(["python", "visage.py"])
    else:
       	sauvegarder_keypoints(output_folder, video_path, frames_number)

def select_file(entry_widget):
    global video_path
    video_path = tk.filedialog.askopenfilename()
    entry_widget.delete(0, tk.END)
    entry_widget.insert(0, video_path)

def select_folder(entry_widget):
    global output_folder
    output_folder = tk.filedialog.askdirectory()
    entry_widget.delete(0, tk.END)
    entry_widget.insert(0, output_folder)

def main():
    global selected_part, body_checkbox, head_checkbox, run_button  # Déclaration globale des variables

    root = tk.Tk()
    root.title("Pose Estimation")

    selected_part = tk.StringVar(value="corps")  # Par défaut, la case corps est cochée

    # Body Checkbox
    body_checkbox = tk.Checkbutton(root, text="Corps", variable=selected_part, onvalue="corps", offvalue="tête", command=toggle_part)
    body_checkbox.grid(row=3, column=1)

    # Video Path
    video_path_label = tk.Label(root, text="Video Path:")
    video_path_label.grid(row=0, column=0)
    video_path_entry = tk.Entry(root, width=50)
    video_path_entry.grid(row=0, column=1)
    video_path_button = tk.Button(root, text="Browse", command=lambda: select_file(video_path_entry))
    video_path_button.grid(row=0, column=2)

    # Output Folder
    output_folder_label = tk.Label(root, text="Output Folder:")
    output_folder_label.grid(row=1, column=0)
    output_folder_entry = tk.Entry(root, width=50)
    output_folder_entry.grid(row=1, column=1)
    output_folder_button = tk.Button(root, text="Browse", command=lambda: select_folder(output_folder_entry))
    output_folder_button.grid(row=1, column=2)

    # Frames Number
    frames_number_label = tk.Label(root, text="Frames Number:")
    frames_number_label.grid(row=2, column=0)
    frames_number_entry = tk.Entry(root, width=50)
    frames_number_entry.grid(row=2, column=1)

    # Head Checkbox
    head_checkbox = tk.Checkbutton(root, text="Tête", variable=selected_part, onvalue="tête", offvalue="corps", command=toggle_part)
    head_checkbox.grid(row=3, column=0)

    video_path = video_path_entry.get()

    # Run Button
    run_button = tk.Button(root, text="Run", command=lambda: run_application(video_path_entry.get(), output_folder_entry.get(), frames_number_entry.get(), selected_part.get()))
    run_button.grid(row=4, column=1)

    root.mainloop()

if __name__ == "__main__":
    main()