SELECT 
    sourcils_gauche_1_X,
    sourcils_gauche_1_Y,
    sourcils_gauche_2_X,
    sourcils_gauche_2_Y,
    sourcils_gauche_3_X,
    sourcils_gauche_3_Y,
    sourcils_gauche_4_X,
    sourcils_gauche_4_Y,
    sourcils_gauche_5_X,
    sourcils_gauche_5_Y,
FROM {{ref('face_staging')}}
    