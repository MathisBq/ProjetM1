SELECT 
    dist_ouverture_oeil_gauche,
    Oeil_gauche,
    Regard_gauche,
    oeil_gauche_1_X,
    oeil_gauche_1_Y,
    oeil_gauche_2_X,
    oeil_gauche_2_Y,
    oeil_gauche_3_X,
    oeil_gauche_3_Y,
    oeil_gauche_4_X,
    oeil_gauche_4_Y,
    oeil_gauche_5_X,
    oeil_gauche_5_Y,
    oeil_gauche_6_X,
    oeil_gauche_6_Y
FROM {{ref('face_staging')}}