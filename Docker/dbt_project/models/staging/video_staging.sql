-- models/video_staging.sql

SELECT
	id,
	nom,
	temps,
	nb_pers,
	format,
	path
FROM {{ source('data_source', 'vidéos') }}