SELECT
    nez_1_X,
    nez_1_Y,
    nez_2_X,
    nez_2_Y,
    nez_3_X,
    nez_3_Y,
    nez_4_X,
    nez_4_Y,
    nez_5_X,
    nez_5_Y,
    nez_6_X,
    nez_6_Y,
    nez_7_X,
    nez_7_Y,
    nez_8_X,
    nez_8_Y,
    nez_9_X,
    nez_9_Y,
FROM {{ref('face_staging')}}