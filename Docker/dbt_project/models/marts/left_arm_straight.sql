SELECT * 
FROM {{ref('fct_left_arm')}}
WHERE (epaule_gauche_X = coude_gauche_X AND coude_gauche_X = poignet_gauche_X)
   OR (epaule_gauche_Y = coude_gauche_Y AND coude_gauche_Y = poignet_gauche_Y);
