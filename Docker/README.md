# Ingénieurie de données, estimation de poseet informatique affective 

## Introduction

Le but de ce projet est de traiter des données à partir d'un squelette ou de visages pour permettre de les visualiser et de les utiliser dans des applications tierces.

## Démarrage du projet
Pré requis :Docker et Docker-compose installés sur votre machine.

Lien pour installer Docker : 

```
https://docs.docker.com/desktop/install/windows-install/
```

Pour démarrer le projet, utilisez le script `run.sh` :


```bash
./run.sh
```

# Accès aux données
Pour accéder à l'interface PgAdmin, ouvrez votre navigateur et accédez à l'URL de pgAdmin: http://localhost:5050
- username : admin@admin.com
- password : root

Les données sont accessibles via la base de donnée postgresql. Vous pouvez accéder à la base de donnée avec les identifiants suivants :
- host : db
- port : 5432
- username : root
- password : root
- database : postgres

### Première utilisation sur une nouvelle machine
Pour les nouvelles machines, il est nécessaire de se connecter à Airbyte au moins une fois pour recevoir correctement les configurations. Après avoir démarré le projet avec le script run.sh, ouvrez votre navigateur et accédez à l'URL d'Airbyte (généralement http://localhost:8000). Pour vous connecter à Airbyte, utilisez les identifiants suivants :
- username : airbyte
- password : password

Ensuite il faut entrer des informations tel que l'email de l'entreprise et le nom (elles n'auront aucune importance). Une fois sur l'écran principal d'Airbyte vous verez que les configurations ne sont pas présentes. 

## Configuration des sources de données
## Google Sheets
Les données viennent d'une source tableur Google Sheets.

vous pouvez générer votre clé API depuis le site [Google Cloud](https://console.cloud.google.com/apis/credentials/serviceaccountkey) en suivant le tutoriel de Airbyte [ici](https://docs.airbyte.io/integrations/sources/google-sheets).

Il suffit l'url du fichier Google Sheets dans le champ correspondant.

## Configuration des destination
## PostgreSQL

Avant de remplir les différents champs, entrez ces commandes dans le terminal de pgAdmin :

```sql
CREATE USER airbyte_user WITH PASSWORD 'password';
GRANT CREATE, TEMPORARY ON DATABASE <database> TO airbyte_user;
```

- Destination name : "entrer le nom que vous souhaitez lui donner"
- Host : localhost
- Port : 5432
- DB Name : postgres
- Default Schema : public
- User : airbyte_user
- SSL Mode : prefer
- SSH Tunnel Method : No Tunnel
- Password : password

Appuyez maintenant sur le bouton "Set up destination".

## Configuration de la connexion

Vous pouvez choisir le mode de transmission, manuel ou automatique

Enfin pour exécuter la tranmission des données entre la source et la destination appuyez sur le bouton "Sync now".





# Structure des fichiers
Tout est fait pour suivre la structure du star schema suivant:
![Star schema](Star_Schema.png)