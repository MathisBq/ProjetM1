SELECT 
    sourcils_droit_1_X,
    sourcils_droit_1_Y,
    sourcils_droit_2_X,
    sourcils_droit_2_Y,
    sourcils_droit_3_X,
    sourcils_droit_3_Y,
    sourcils_droit_4_X,
    sourcils_droit_4_Y,
    sourcils_droit_5_X,
    sourcils_droit_5_Y,
FROM {{ref('face_staging')}}