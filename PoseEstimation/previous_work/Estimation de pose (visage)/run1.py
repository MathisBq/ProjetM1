#!/usr/bin/env python
# coding: utf-8

# ## Head_pose_detection_google_sheets

# In[9]:


import numpy as np
import cv2
import mediapipe as mp
import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Initialisation de la connexion avec Google Sheets
scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('projetm1-417108-4f9a10581282.json', scope)
client = gspread.authorize(credentials)
sheet = client.open_by_key('1Or2WKRekFkoEeb6PlFUOKT0ppKm3xKqlY2HAKdcbp5c').worksheet('tete')  # Remplacez YOUR_GOOGLE_SHEET_KEY par la clé de votre feuille Google Sheets

mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh(static_image_mode=True, min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=4)

headers = ['Frame','Visage','pose_tete','pose_tete_x','pose_tete_y','pose_tete_z']
# Écrire les en-têtes dans la feuille Google Sheets
sheet.append_row(headers)

def process_frame(image, frame_index, file_index):
    results = face_mesh.process(image)

    face_infos = []  # Liste pour stocker les informations de chaque visage détecté

    if results.multi_face_landmarks:
        for face_idx, face_landmarks in enumerate(results.multi_face_landmarks):
            face_2d = []
            face_3d = []

            for idx, lm in enumerate(face_landmarks.landmark):
                if idx == 33 or idx == 263 or idx == 1 or idx == 61 or idx == 291 or idx == 199:
                    if idx == 1:
                        nose_2d = (lm.x * image.shape[1], lm.y * image.shape[0])
                        nose_3d = (lm.x * image.shape[1], lm.y * image.shape[0], lm.z * 3000)
                    x, y = int(lm.x * image.shape[1]), int(lm.y * image.shape[0])
                    face_2d.append([x, y])
                    face_3d.append(([x, y, lm.z]))

            face_2d = np.array(face_2d, dtype=np.float64)
            face_3d = np.array(face_3d, dtype=np.float64)

            focal_length = 1 * image.shape[1]
            cam_matrix = np.array([[focal_length, 0, image.shape[0] / 2],
                                    [0, focal_length, image.shape[1] / 2],
                                    [0, 0, 1]])
            distortion_matrix = np.zeros((4, 1), dtype=np.float64)

            success, rotation_vec, translation_vec = cv2.solvePnP(face_3d, face_2d, cam_matrix, distortion_matrix)

            rmat, _ = cv2.Rodrigues(rotation_vec)
            angles, _, _, _, _, _ = cv2.RQDecomp3x3(rmat)

            x = angles[0] * 360
            y = angles[1] * 360
            z = angles[2] * 360

            # Détermination de la direction de la tête
            pose_tete = ""
            if y < -5:
                pose_tete = "Regarde à gauche"
            elif y > 5:
                pose_tete = "Regarde à droite"
            elif x < -5:
                pose_tete = "Regarde en bas"
            elif x > 5:
                pose_tete = "Regarde en haut"
            else:
                pose_tete = "Regarde droit devant"

            # Stockage des informations dans la feuille Google Sheets
            row = [frame_index, "Visage " + str(face_idx + 1), pose_tete, x, y, z]
            sheet.append_row(row)

# Lecture de la vidéo
cap = cv2.VideoCapture('C:\\Users\\Etudiant\\projet_per\\Eyes-Tracking-Opencv-and-Dlib-master\\Videos\\vid2.mp4')

frame_index = 0
while cap.isOpened():
    success, image = cap.read()

    if not success:
        break

    process_frame(image, frame_index, frame_index)

    frame_index += 1

cap.release()
cv2.destroyAllWindows()


# In[ ]:




