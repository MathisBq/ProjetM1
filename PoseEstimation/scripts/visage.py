#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2 as cv
import numpy as np
import dlib
import math
from corps import video_path

# variables
fonts = cv.FONT_HERSHEY_COMPLEX

# colors
YELLOW = (0, 247, 255)
CYAN = (255, 255, 0)
MAGENTA = (255, 0, 242)
GOLDEN = (32, 218, 165)
LIGHT_BLUE = (255, 9, 2)
PURPLE = (128, 0, 128)
CHOCOLATE = (30, 105, 210)
PINK = (147, 20, 255)
ORANGE = (0, 69, 255)
GREEN = (0, 255, 0)
LIGHT_GREEN = (0, 255, 13)
LIGHT_CYAN = (255, 204, 0)
BLUE = (255, 0, 0)
RED = (0, 0, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
LIGHT_RED = (2, 53, 255)


# face detector object
detectFace = dlib.get_frontal_face_detector()
# landmarks detector
predictor = dlib.shape_predictor(
    "shape_predictor_68_face_landmarks.dat")

# function


def midpoint(pts1, pts2):
    x, y = pts1
    x1, y1 = pts2
    xOut = int((x + x1)/2)
    yOut = int((y1 + y)/2)
    # print(xOut, x, x1)
    return (xOut, yOut)


def eucaldainDistance(pts1, pts2):
    x, y = pts1
    x1, y1 = pts2
    eucaldainDist = math.sqrt((x1 - x) ** 2 + (y1 - y) ** 2)

    return eucaldainDist

# creating face detector function


def faceDetector(image, gray, Draw=True):
    cordFace1 = (0, 0)
    cordFace2 = (0, 0)
    # getting faces from face detector
    faces = detectFace(gray)

    face = None
    # looping through All the face detected.
    for face in faces:
        # getting coordinates of face.
        cordFace1 = (face.left(), face.top())
        cordFace2 = (face.right(), face.bottom())

        # draw rectangle if draw is True.
        if Draw == True:
            cv.rectangle(image, cordFace1, cordFace2, GREEN, 2)
    return image, face


def faceLandmakDetector(image, gray, face, Draw=True):
    # calling the landmarks predictor
    landmarks = predictor(gray, face)
    pointList = []
    # looping through each landmark
    for n in range(0, 68):
        point = (landmarks.part(n).x, landmarks.part(n).y)
        # getting x and y coordinates of each mark and adding into list.
        pointList.append(point)
        # draw if draw is True.
        if Draw == True:
            # draw circle on each landmark
            cv.circle(image, point, 3, ORANGE, 1)
    return image, pointList

# Blink detector function.


def blinkDetector(eyePoints):
    top = eyePoints[1:3]
    bottom = eyePoints[4:6]
    # finding the mid point of above points
    topMid = midpoint(top[0], top[1])
    bottomMid = midpoint(bottom[0], bottom[1])
    # getting the actual width and height eyes using eucaldainDistance function
    VerticalDistance = eucaldainDistance(topMid, bottomMid)
    HorizontalDistance = eucaldainDistance(eyePoints[0], eyePoints[3])
    # print()

    blinkRatio = (HorizontalDistance/VerticalDistance)
    return blinkRatio, topMid, bottomMid

# Eyes Tracking function.


def EyeTracking(image, gray, eyePoints):
    # getting dimensions of image
    dim = gray.shape
    # creating mask .
    mask = np.zeros(dim, dtype=np.uint8)

    # converting eyePoints into Numpy arrays.
    PollyPoints = np.array(eyePoints, dtype=np.int32)
    # Filling the Eyes portion with WHITE color.
    cv.fillPoly(mask, [PollyPoints], 255)

    # Writing gray image where color is White  in the mask using Bitwise and operator.
    eyeImage = cv.bitwise_and(gray, gray, mask=mask)

    # getting the max and min points of eye inorder to crop the eyes from Eye image .

    maxX = (max(eyePoints, key=lambda item: item[0]))[0]
    minX = (min(eyePoints, key=lambda item: item[0]))[0]
    maxY = (max(eyePoints, key=lambda item: item[1]))[1]
    minY = (min(eyePoints, key=lambda item: item[1]))[1]

    # other then eye area will black, making it white
    eyeImage[mask == 0] = 255

    # cropping the eye form eyeImage.
    cropedEye = eyeImage[minY:maxY, minX:maxX]

    # getting width and height of cropedEye
    height, width = cropedEye.shape

    divPart = int(width/3)

    #  applying the threshold to the eye .
    ret, thresholdEye = cv.threshold(cropedEye, 100, 255, cv.THRESH_BINARY)

    # dividing the eye into Three parts .
    rightPart = thresholdEye[0:height, 0:divPart]
    centerPart = thresholdEye[0:height, divPart:divPart+divPart]
    leftPart = thresholdEye[0:height, divPart+divPart:width]

    # counting Black pixel in each part using numpy.
    rightBlackPx = np.sum(rightPart == 0)
    centerBlackPx = np.sum(centerPart == 0)
    leftBlackPx = np.sum(leftPart == 0)
    pos, color = Position([rightBlackPx, centerBlackPx, leftBlackPx])
    # print(pos)

    return mask, pos, color


def Position(ValuesList):

    maxIndex = ValuesList.index(max(ValuesList))
    posEye = ''
    color = [WHITE, BLACK]
    if maxIndex == 0:
        posEye = "Right"
        color = [YELLOW, BLACK]
    elif maxIndex == 1:
        posEye = "Center"
        color = [BLACK, MAGENTA]
    elif maxIndex == 2:
        posEye = "Left"
        color = [LIGHT_CYAN, BLACK]
    else:
        posEye = "Eye Closed"
        color = [BLACK, WHITE]
    return posEye, color

# code Validé
import cv2 as cv
import dlib
import math
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import time
from datetime import datetime
import numpy as np  # Ajout de l'importation de numpy



import cv2
import mediapipe as mp
import os


# Fonction EyeTracking
# Fonction EyeTracking
def EyeTracking(image, gray, eyePoints):
    # getting dimensions of image
    dim = gray.shape
    # creating mask .
    mask = np.zeros(dim, dtype=np.uint8)

    # converting eyePoints into Numpy arrays.
    PollyPoints = np.array(eyePoints, dtype=np.int32)
    # Filling the Eyes portion with WHITE color.
    cv.fillPoly(mask, [PollyPoints], 255)

    # Writing gray image where color is White  in the mask using Bitwise and operator.
    eyeImage = cv.bitwise_and(gray, gray, mask=mask)

    # getting the max and min points of eye inorder to crop the eyes from Eye image .

    maxX = (max(eyePoints, key=lambda item: item[0]))[0]
    minX = (min(eyePoints, key=lambda item: item[0]))[0]
    maxY = (max(eyePoints, key=lambda item: item[1]))[1]
    minY = (min(eyePoints, key=lambda item: item[1]))[1]

    # other then eye area will black, making it white
    eyeImage[mask == 0] = 255

    # cropping the eye form eyeImage.
    cropedEye = eyeImage[minY:maxY, minX:maxX]

    # getting width and height of cropedEye
    height, width = cropedEye.shape

    divPart = int(width/3)

    #  applying the threshold to the eye .
    ret, thresholdEye = cv.threshold(cropedEye, 100, 255, cv.THRESH_BINARY)

    # dividing the eye into Three parts .
    rightPart = thresholdEye[0:height, 0:divPart]
    centerPart = thresholdEye[0:height, divPart:divPart+divPart]
    leftPart = thresholdEye[0:height, divPart+divPart:width]

    # counting Black pixel in each part using numpy.
    rightBlackPx = np.sum(rightPart == 0)
    centerBlackPx = np.sum(centerPart == 0)
    leftBlackPx = np.sum(leftPart == 0)
    pos, color = Position([rightBlackPx, centerBlackPx, leftBlackPx])
    # print(pos)

    return mask, pos, color, pos  # Retourne les valeurs nécessaires, y compris gaze_direction


# Initialiser le détecteur de visages et le prédicteur de landmarks
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# Seuils pour détecter un clignement et compteurs de clignement
blink_threshold = 5.0
blink_state_right = False
blink_state_left = False

# Charger la vidéo
videoPath = video_path # récupérer a partir du script launcher.py
cap = cv.VideoCapture(videoPath)

video_name = videoPath.split('/')[-1].split('.')[0]
idvideo = video_name[:2] + video_name[-1]
# Extraire les deux premières lettres et la dernière lettre du nom du fichier vidéo
idvideo = video_name[:2] + video_name[-1]
# Obtenir la fréquence d'images par seconde de la vidéo
fps = cap.get(cv.CAP_PROP_FPS)

# Authentification avec Google Sheets
scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('projetm1-417108-4f9a10581282.json', scope)
client = gspread.authorize(credentials)
sheet = client.open_by_key('1Or2WKRekFkoEeb6PlFUOKT0ppKm3xKqlY2HAKdcbp5c').worksheet('visage')
info_sheet = client.open_by_key('1Or2WKRekFkoEeb6PlFUOKT0ppKm3xKqlY2HAKdcbp5c').worksheet('vidéos')

video_duration = cap.get(cv.CAP_PROP_FRAME_COUNT) / fps
video_format = videoPath.split('.')[-1]
video_path = videoPath

mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh(static_image_mode=True, min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=4)

# Nombre de personnes détectées dans la vidéo (à adapter selon votre logique)
nb_per = "2"

# Stocker les informations dans la feuille Google Sheets
info_row = [idvideo, video_name, video_duration,nb_per, video_format, video_path]
info_sheet.append_row(info_row)

frame_counter = 0

# Créer les en-têtes pour les colonnes
headers = ['frame', 'Visage', 'Moment', 'idvideo', 'dist_ouverture_oeil_droit', 'Oeil droit', 'dist_ouverture_oeil_gauche', 'Oeil gauche', 'Regard droit', 'Regard gauche','pose_tete','pose_tete_x','pose_tete_y','pose_tete_z']

# Ajouter les en-têtes pour les différents points selon vos spécifications
headers.extend(['tempe_droit_1_X', 'tempe_droit_1_Y', 'tempe_droit_2_X', 'tempe_droit_2_Y'])
headers.extend(['joues_droit_1_X', 'joues_droit_1_Y', 'joues_droit_2_X', 'joues_droit_2_Y', 'joues_droit_3_X', 'joues_droit_3_Y', 'joues_droit_4_X', 'joues_droit_4_Y'])
headers.extend(['menton_1_X', 'menton_1_Y', 'menton_2_X', 'menton_2_Y', 'menton_3_X', 'menton_3_Y', 'menton_4_X', 'menton_4_Y', 'menton_5_X', 'menton_5_Y'])
headers.extend(['joues_gauche_1_X', 'joues_gauche_1_Y', 'joues_gauche_2_X', 'joues_gauche_2_Y', 'joues_gauche_3_X', 'joues_gauche_3_Y', 'joues_gauche_4_X', 'joues_gauche_4_Y'])
headers.extend(['tempe_gauche_1_X', 'tempe_gauche_1_Y', 'tempe_gauche_2_X', 'tempe_gauche_2_Y'])
headers.extend(['sourcils_droit_1_X', 'sourcils_droit_1_Y', 'sourcils_droit_2_X', 'sourcils_droit_2_Y', 'sourcils_droit_3_X', 'sourcils_droit_3_Y', 'sourcils_droit_4_X', 'sourcils_droit_4_Y', 'sourcils_droit_5_X', 'sourcils_droit_5_Y'])
headers.extend(['sourcils_gauche_1_X', 'sourcils_gauche_1_Y', 'sourcils_gauche_2_X', 'sourcils_gauche_2_Y', 'sourcils_gauche_3_X', 'sourcils_gauche_3_Y', 'sourcils_gauche_4_X', 'sourcils_gauche_4_Y', 'sourcils_gauche_5_X', 'sourcils_gauche_5_Y'])
headers.extend(['nez_1_X', 'nez_1_Y', 'nez_2_X', 'nez_2_Y', 'nez_3_X', 'nez_3_Y', 'nez_4_X', 'nez_4_Y', 'nez_5_X', 'nez_5_Y','nez_6_X', 'nez_6_Y','nez_7_X', 'nez_7_Y','nez_8_X', 'nez_8_Y','nez_9_X', 'nez_9_Y'])
headers.extend(['oeil_droit_1_X', 'oeil_droit_1_Y', 'oeil_droit_2_X', 'oeil_droit_2_Y', 'oeil_droit_3_X', 'oeil_droit_3_Y', 'oeil_droit_4_X', 'oeil_droit_4_Y', 'oeil_droit_5_X', 'oeil_droit_5_Y', 'oeil_droit_6_X', 'oeil_droit_6_Y'])
headers.extend(['oeil_gauche_1_X', 'oeil_gauche_1_Y', 'oeil_gauche_2_X', 'oeil_gauche_2_Y', 'oeil_gauche_3_X', 'oeil_gauche_3_Y', 'oeil_gauche_4_X', 'oeil_gauche_4_Y', 'oeil_gauche_5_X', 'oeil_gauche_5_Y','oeil_gauche_6_X', 'oeil_gauche_6_Y'])
headers.extend(['levre_sup_1_X', 'levre_sup_1_Y', 'levre_sup_2_X', 'levre_sup_2_Y', 'levre_sup_3_X', 'levre_sup_3_Y', 'levre_sup_4_X', 'levre_sup_4_Y', 'levre_sup_5_X', 'levre_sup_5_Y', 'levre_sup_6_X', 'levre_sup_6_Y', 'levre_sup_7_X', 'levre_sup_7_Y'])
headers.extend(['levre_inf_1_X', 'levre_inf_1_Y', 'levre_inf_2_X', 'levre_inf_2_Y', 'levre_inf_3_X', 'levre_inf_3_Y', 'levre_inf_4_X', 'levre_inf_4_Y', 'levre_inf_5_X', 'levre_inf_5_Y'])
headers.extend(['levre_sup_8_X', 'levre_sup_8_Y', 'levre_sup_9_X', 'levre_sup_9_Y', 'levre_sup_10_X', 'levre_sup_10_Y', 'levre_sup_11_X', 'levre_sup_11_Y', 'levre_sup_12_X', 'levre_sup_12_Y'])
headers.extend(['levre_inf_6_X', 'levre_inf_6_Y', 'levre_inf_7_X', 'levre_inf_7_Y', 'levre_inf_8_X', 'levre_inf_8_Y'])
# Écrire les en-têtes dans la feuille Google Sheets
sheet.append_row(headers)

def process_frame(image, frame_index, file_index):
    results = face_mesh.process(image)

    face_infos = []  # Liste pour stocker les informations de chaque visage détecté

    if results.multi_face_landmarks:
        for face_idx, face_landmarks in enumerate(results.multi_face_landmarks):
            face_2d = []
            face_3d = []

            for idx, lm in enumerate(face_landmarks.landmark):
                if idx == 33 or idx == 263 or idx == 1 or idx == 61 or idx == 291 or idx == 199:
                    if idx == 1:
                        nose_2d = (lm.x * image.shape[1], lm.y * image.shape[0])
                        nose_3d = (lm.x * image.shape[1], lm.y * image.shape[0], lm.z * 3000)
                    x, y = int(lm.x * image.shape[1]), int(lm.y * image.shape[0])
                    face_2d.append([x, y])
                    face_3d.append(([x, y, lm.z]))

            face_2d = np.array(face_2d, dtype=np.float64)
            face_3d = np.array(face_3d, dtype=np.float64)

            focal_length = 1 * image.shape[1]
            cam_matrix = np.array([[focal_length, 0, image.shape[0] / 2],
                                    [0, focal_length, image.shape[1] / 2],
                                    [0, 0, 1]])
            distortion_matrix = np.zeros((4, 1), dtype=np.float64)

            success, rotation_vec, translation_vec = cv2.solvePnP(face_3d, face_2d, cam_matrix, distortion_matrix)

            rmat, _ = cv2.Rodrigues(rotation_vec)
            angles, _, _, _, _, _ = cv2.RQDecomp3x3(rmat)

            x = angles[0] * 360
            y = angles[1] * 360
            z = angles[2] * 360

            # Détermination de la direction de la tête
            pose_tete = ""
            if y < -5:
                pose_tete = "Regarde à gauche"
            elif y > 5:
                pose_tete = "Regarde à droite"
            elif x < -5:
                pose_tete = "Regarde en bas"
            elif x > 5:
                pose_tete = "Regarde en haut"
            else:
                pose_tete = "Regarde droit devant"

    return pose_tete, x, y, z        

frame_index = 0  # Initialisation de l'indice de frame

while True:
    ret, frame = cap.read()
    if not ret:
        break

    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    faces = detector(gray_frame)
    
    # Dans la boucle principale
    for idx, face in enumerate(faces):
        # Obtenir les landmarks pour chaque visage
        landmarks = predictor(gray_frame, face)
        
        # Processus de calcul des données et des coordonnées
        frame_time = frame_index / fps
        RightEyePoints = [(landmarks.part(n).x, landmarks.part(n).y) for n in range(36, 42)]
        LeftEyePoints = [(landmarks.part(n).x, landmarks.part(n).y) for n in range(42, 48)]
        mask_right, pos_right, color_right, gaze_direction_right = EyeTracking(frame, gray_frame, RightEyePoints)
        mask_left, pos_left, color_left, gaze_direction_left = EyeTracking(frame, gray_frame, LeftEyePoints)
        blinkRatioRight, _, _ = blinkDetector(RightEyePoints)
        blinkRatioLeft, _, _ = blinkDetector(LeftEyePoints)
        eye_state_right = "Fermé" if blinkRatioRight > blink_threshold else "Ouvert"
        eye_state_left = "Fermé" if blinkRatioLeft > blink_threshold else "Ouvert"
        frame_time_str = "{:.2f}".format(frame_time)
        blinkRatioRight_str = "{:.2f}".format(blinkRatioRight)
        blinkRatioLeft_str = "{:.2f}".format(blinkRatioLeft)
        
        pose_tete, x, y, z = process_frame(frame, frame_index, idx)  # Appel de la fonction process_frame
        
        row = [frame_index, idx+1, frame_time_str, idvideo, blinkRatioRight_str, eye_state_right, blinkRatioLeft_str, eye_state_left, gaze_direction_right, gaze_direction_left, pose_tete, x, y, z]
        # Appeler la fonction pour traiter cette frame
        
        # Ajouter les coordonnées des landmarks à la ligne
        for i in range(68):
            x, y = landmarks.part(i).x, landmarks.part(i).y
            row.append(x)
            row.append(y)
        
        # Ajouter la ligne à la feuille Google Sheets
        sheet.append_row(row)

        frame_index += 1  # Incrémenter l'indice de frame


# Libération des ressources
cap.release()

print("Les résultats ont été enregistrés dans la feuille Google Sheets.")


# In[ ]:




