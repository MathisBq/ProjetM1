SELECT * 
FROM {{ref('fct_right_arm')}}
WHERE (epaule_droite_X = coude_droit_X AND coude_droit_X = poignet_droit_X)
   OR (epaule_droite_Y = coude_droit_Y AND coude_droit_Y = poignet_droit_Y);
