SELECT 
    dist_ouverture_oeil_droit,
    Oeil_droit,
    Regard_droit,
    oeil_droit_1_X,
    oeil_droit_1_Y,
    oeil_droit_2_X,
    oeil_droit_2_Y,
    oeil_droit_3_X,
    oeil_droit_3_Y,
    oeil_droit_4_X,
    oeil_droit_4_Y,
    oeil_droit_5_X,
    oeil_droit_5_Y,
    oeil_droit_6_X,
    oeil_droit_6_Y
FROM {{ref('face_staging')}}